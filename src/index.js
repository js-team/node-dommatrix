/** @typedef {import('../types/index')} */
import CSSMatrix from './dommatrix';
import Version from './version';

Object.assign(CSSMatrix, { Version });

export default CSSMatrix;
