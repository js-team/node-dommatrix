export as namespace CSSM;
export default CSSMatrix;

export { PointTuple, JSONMatrix, matrix, matrix3d } from './more/types';

import { default as CSSMatrix } from 'dommatrix/src/dommatrix';
